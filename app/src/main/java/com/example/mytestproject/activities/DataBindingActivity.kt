package com.example.mytestproject.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.example.mytestproject.R
import com.example.mytestproject.databinding.ActivityDataBindingBinding
import com.example.mytestproject.modals.Students

class DataBindingActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding:ActivityDataBindingBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_data_binding)
        binding.btnShow.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        binding.students=Students("Ankit Singh","Android Developer")
    }
}